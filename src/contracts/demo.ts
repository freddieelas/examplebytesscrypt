import {
    assert,
    ByteString,
    method,
    prop,
    SmartContract,
    toByteString,
} from 'scrypt-ts'

export class Demo extends SmartContract {
    @prop()
    testBytes: ByteString

    constructor() {
        super(...arguments)
        this.testBytes = toByteString('deadbeef',false)
    }

    @method()
    public unlock() {
        assert(this.testBytes.slice(0,2)===toByteString('dead',false))
    }
}
