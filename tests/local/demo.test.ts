import { expect, use } from 'chai'
import { MethodCallOptions, sha256, toByteString } from 'scrypt-ts'
import { Demo } from '../../src/contracts/demo'
import { getDummySigner, getDummyUTXO } from '../utils/txHelper'
import chaiAsPromised from 'chai-as-promised'
use(chaiAsPromised)

describe('Test SmartContract `Demo`', () => {
    let instance: Demo

    before(async () => {
        await Demo.compile()
        instance = new Demo()
        await instance.connect(getDummySigner())
    })

    it('should pass the public method unit test successfully.', async () => {
        const { tx: callTx, atInputIndex } = await instance.methods.unlock(
            {
                fromUTXO: getDummyUTXO(),
            } as MethodCallOptions<Demo>
        )

        const result = callTx.verifyScript(atInputIndex)
        expect(result.success, result.error).to.eq(true)
    })

    it('should throw with wrong message.', async () => {
        return expect(
            instance.methods.unlock({
                fromUTXO: getDummyUTXO(),
            } as MethodCallOptions<Demo>)
        ).to.be.rejectedWith(/Hash does not match/)
    })
})
